'use strict';

/**
 * Directive for user form
 * @author: Yael Raifer
 *
 */

angular.module('guardicoreInstallerApp').directive('progressIndicator', function(STEPS){

  return{
    restrict : 'E',
    scope: {
      onReset: '=',
      currentPosition: '=',
      currentLength: '='
    },
    templateUrl: 'views/directives/progress-indicator.html',
    controller: function($scope) {

      $scope.steps = STEPS;

      $scope.setProgressBarTransition = function(index) {
        return {
          transitionDelay: 0.2 * ($scope.currentLength - index) + 's',
          transitionDuration: '0.2s',
          transitionTimingFunction: 'linear'
        };
      };

    }
  };

});
