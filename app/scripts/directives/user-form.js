'use strict';

/**
 * Directive for user form
 * @author: Yael Raifer
 *
 */

angular.module('guardicoreInstallerApp').directive('userForm', function(ROLES){
  return{
    restrict : 'E',
    scope: {
      user: '=',
      onClose: '&'
    },
    templateUrl: 'views/directives/user-form.html',
    controller: function($scope){

      $scope.roles = ROLES;
      $scope.user = $scope.user || {};
      $scope.closed = $scope.user.valid;

      $scope.user.selectedRole = $scope.user.selectedRole || $scope.roles[0];

      $scope.closeUser = function(){
        if (isValid()){
          $scope.user.valid = true;
          $scope.closed = true;
          $scope.onClose();
        } else {
          $scope.user.valid = false;
          alert('please complete the form');
        }
      };

      $scope.editUser = function(){
        $scope.closed = false;
      };

      function isValid(){
        return ($scope.user.userName && $scope.user.password && $scope.user.password === $scope.user.passwordConfirm);
      }
    }
  }
});
