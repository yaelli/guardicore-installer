'use strict';

/**
 * Directive for submit button
 * @author: Yael Raifer
 *
 */

angular.module('guardicoreInstallerApp').directive('submitButton', function(UserInput, STEPS){
  return {
    restrict: 'E',
    scope: {
      params: '='
    },
    template: '<div class="submit-button"><button class="btn" ng-class="isLast ? \'btn-warning\' : \'btn-primary\'" ng-click="onSubmit()">{{text}}</button></div>',
    controller: function ($scope, $state) {

      var state = $state.current.name;
      var slideIndex = UserInput.getIndex();
      $scope.isLast = slideIndex + 1  === STEPS.length;

      $scope.text = $scope.isLast ? 'Finish' : 'Next -->';

      $scope.onSubmit = function(){
        if ($scope.isLast){
          finish();
        } else{
          next();
        }
      };

      function finish(){
        if(UserInput.save[state]){
          UserInput.save[state]($scope.params);
        }
        UserInput.finishInstallation();
      }

      function next(){
        if(UserInput.save[state]){
          UserInput.save[state]($scope.params);
        }
        UserInput.next();
        $state.go(STEPS[UserInput.getIndex()]);
      }

    }
  }
});
