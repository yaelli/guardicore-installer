'use strict';

/**
 * @ngdoc function
 * @name guardicoreInstallerApp.controller:MainCtrl
 * @description
 * # ParentCtrl
 * Controller of the guardicoreInstallerApp
 */
angular.module('guardicoreInstallerApp')
  .controller('ParentCtrl', function ($scope, UserInput, STEPS, $state, $timeout) {

    $state.go(STEPS[0]);

    $scope.reset = function(){
      $scope.length = $scope.index;
      $scope.onReset = true;
      UserInput.reset();
      $state.go(STEPS[0]);
      $timeout(function(){
        $scope.onReset = false;
      }, 500);
    };

    $scope.$on('$stateChangeSuccess', function(event, next, current) {

      $scope.index = UserInput.getIndex();
      $scope.nextIndex = STEPS.indexOf(next.name);
      if ($scope.nextIndex !== -1 && $scope.index !== $scope.nextIndex){
        $state.go(STEPS[$scope.index]);
      }

    });

  });
