'use strict';

/**
 * @ngdoc function
 * @name guardicoreInstallerApp.controller:MainCtrl
 * @description
 * # usersManagerCtrl
 * Controller of the guardicoreInstallerApp
 */
angular.module('guardicoreInstallerApp')
  .controller('usersManagerCtrl', function ($scope, UserInput) {

    $scope.users = UserInput.getUsers() || [{}];

    $scope.addUser = function(lastUser){
      if (lastUser){
        $scope.users.push({});
      }
    };

  });
