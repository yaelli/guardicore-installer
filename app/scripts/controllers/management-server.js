'use strict';

/**
 * @ngdoc function
 * @name guardicoreInstallerApp.controller:MainCtrl
 * @description
 * # managementServerCtrl
 * Controller of the guardicoreInstallerApp
 */
angular.module('guardicoreInstallerApp')
  .controller('managementServerCtrl', function ($scope, SystemInfo) {

    SystemInfo.getDirectories(function(directories){
      $scope.directories = directories;
      $scope.chosenDirectory = $scope.directories[0];
    });

  });
