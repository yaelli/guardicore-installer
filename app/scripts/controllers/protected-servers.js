'use strict';

/**
 * @ngdoc function
 * @name guardicoreInstallerApp.controller:MainCtrl
 * @description
 * # protectedServersCtr
 * Controller of the guardicoreInstallerApp
 */
angular.module('guardicoreInstallerApp')
  .controller('protectedServersCtr', function ($scope, SystemInfo) {

    SystemInfo.getServers(function(servers){
        $scope.servers = servers;
    });

    $scope.user= {};

});
