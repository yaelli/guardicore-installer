'use strict';

/**
 * @ngdoc overview
 * @name guardicoreInstallerApp
 * @description
 * # guardicoreInstallerApp
 *
 * Main module of the application.
 */
angular
  .module('guardicoreInstallerApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ui.router',
    'ngSanitize',
    'checklist-model',
    'ngTouch'
  ])
  .config(function ($urlRouterProvider, $stateProvider) {

    $stateProvider
      .state('managementServer', {
        url: "/managementServer",
        templateUrl: '../views/management-server.html',
        controller: 'managementServerCtrl',
        controllerAs: 'managementServer'
      })
      .state('protectedServers', {
        url: "/protectedServers",
        templateUrl: '../views/protected-servers.html',
        controller: 'protectedServersCtr',
        controllerAs: 'protectedServers'
      })
      .state('usersManager', {
        url: "/usersManager",
        templateUrl: '../views/users-manager.html',
        controller: 'usersManagerCtrl',
        controllerAs: 'usersManager'
      })
      .state('overview', {
        url: "/overview",
        templateUrl: '../views/overview.html',
        controller: 'overviewCtrl',
        controllerAs: 'overview'
      })
      .state('finish', {
        url: "/installation-complete",
        templateUrl: '../views/finish.html',
        controller: 'finishCtrl',
        controllerAs: 'finish'
      });

    $urlRouterProvider.otherwise('/managementServer');

  });
