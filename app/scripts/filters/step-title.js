'use strict';

angular.module('guardicoreInstallerApp').filter('stepTitle', function($filter){
  return function (stateName) {
    switch (stateName) {
      case 'managementServer':
        return 'Set Manager Server';
        break;
      case 'protectedServers':
        return 'Set Servers to Protect';
        break;
      case 'usersManager':
        return 'Set Users';
        break;
      default :
        return $filter('capitalize')(stateName);
        break;
    }
  }
});

angular.module('guardicoreInstallerApp').filter('capitalize', function() {
  return function(input) {
    if (input!=null)
      input = input.toLowerCase();
    return input.substring(0,1).toUpperCase()+input.substring(1);
  }
});
