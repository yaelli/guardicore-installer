'use strict';

/**
 * Service for managing the user's system information
 * @author: Yael Raifer
 *
 */

angular.module('guardicoreInstallerApp').service('SystemInfo', function($resource){


  var servers = null;
  var directories = null;
  var servicesUrl = 'http://localhost:3000/servers';
  var directoriesUrl = 'http://localhost:3000/directories';

  var ServersResource = $resource(servicesUrl, {},
    {
      getServers: {method: 'GET', params: {}, isArray: true}
    }
  );
  var DirectoriesResource = $resource(directoriesUrl, {},
    {
      getDirectories: {method: 'GET', params: {}, isArray: true}
    }
  );

  this.getServers = function(callback){
    if (servers === null) {
      ServersResource.getServers({}, function(response){
        servers = response.map(function(item){
          item.checked = false;
          return item;
        });
        callback(servers);
      });
    } else{
      callback(servers);
    }
  };

  this.getDirectories = function(callback){
    if (directories === null) {
      directories = DirectoriesResource.getDirectories({}, function(){
        callback(directories);
      });
    } else{
      callback(directories);
    }
  };


});
