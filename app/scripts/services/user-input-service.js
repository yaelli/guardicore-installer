'use strict';

/**
 * Service for managing the user's system information
 * @author: Yael Raifer
 *
 */

angular.module('guardicoreInstallerApp').service('UserInput', function($resource, $state) {


  var thisService = this;
  var usersUrl = 'http://localhost:3000/users';
  var managementServerDirectory = null;
  var managerPassword = null;
  var protectedServers = null;
  var users = null;
  var index = 0;

  var UsersResource = $resource(usersUrl, {}, {});

  this.finishInstallation = function () {
    var submitParams = {
      directory : managementServerDirectory,
      managementPassword : managerPassword,
      protectedServers : protectedServers,
      users: users
    };
    thisService.reset();
    UsersResource.save({}, submitParams, function (res) {
      console.log(res);
      $state.go('finish');
    });
  };

  this.next = function(){
    index++;
  };

  this.getIndex = function(){
    return index;
  };

  this.getManagementServerDirectory = function(){
    return managementServerDirectory;
  };

  this.getProtectedServers = function(){
    return protectedServers;
  };

  this.getUsers = function(){
    return users;
  };

  this.reset = function () {
    managementServerDirectory = null;
    managerPassword = null;
    protectedServers = null;
    users = null;
    index = 0;
  };

  this.save = {
    managementServer: function(params){
      managementServerDirectory = params.chosenDirectory;
      if (params.password && params.password === params.passwordConfirm) {
        managerPassword = params.password;
      }
    },
    protectedServers: function(params){
      protectedServers = params.servers;
    },
    usersManager: function(params){
      users = params.users;
    }
  };

});
